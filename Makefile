NPM = npm
WEBPACK = node_modules/.bin/webpack
WEBPACK.SERVER = node_modules/.bin/webpack-dev-server
TMP.DIR = tmp
BIN = node_modules/.bin


nothing:


include etc/Makefile/Makefile.*


start:
	npm start

serve:
	php -S 127.0.0.1:8080 -t build


clean:
	rm -rf build


dist-clean: clean
	rm -rf node_modules


always:
