(define (shouldbe-language lang) (if (string-match "^[a-z]{2}$|([a-z]{2}-[a-z]{2})$" lang) "YES"))
