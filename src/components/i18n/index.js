import 'url-search-params-polyfill';

import i18next from 'i18next';
import XHR from 'i18next-xhr-backend';
import Cache from 'i18next-localstorage-cache';
import LanguageDetector from 'i18next-browser-languagedetector';
import sprintf from 'i18next-sprintf-postprocessor';

import cz from 'etc/lang/cz.po';
import de from 'etc/lang/de.po';
import es from 'etc/lang/es.po';
import fr from 'etc/lang/fr.po';
import hu from 'etc/lang/hu.po';
import it from 'etc/lang/it.po';
import pl from 'etc/lang/pl.po';
import ru from 'etc/lang/ru.po';
import sv from 'etc/lang/sv.po';
import tr from 'etc/lang/tr.po';
import uk from 'etc/lang/uk.po';
import zh from 'etc/lang/zh.po';

i18next
  .use(XHR)
  .use(Cache)
  .use(LanguageDetector)
  .use(sprintf)
  .init({
    overloadTranslationOptionHandler: sprintf.overloadTranslationOptionHandler,
    detection: {
      lookupQuerystring: 'lang',
    },
    load: 'languageOnly',
    wait: false,
    debug: false,
    keySeparator: '-___-',
    nsSeparator: '-____-',
    pluralSeparator: '-_____-',
    contextSeparator: '-______-',
    resources: {
      cz: { translation: cz },
      de: { translation: de },
      fr: { translation: fr },
      hu: { translation: hu },
      it: { translation: it },
      es: { translation: es },
      pl: { translation: pl },
      ru: { translation: ru },
      sv: { translation: sv },
      tr: { translation: tr },
      uk: { translation: uk },
      zh: { translation: zh },
    },
  });

export default i18next;
