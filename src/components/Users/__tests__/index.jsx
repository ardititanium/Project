import React from 'react';
import nock from 'nock';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import users from 'users';
import page1 from 'page1';
import page2 from 'page2';
import page3 from 'page3';

import Users from 'components/Users';
import reducer from 'reducers';
import * as types from 'types/users';

const sleep = new Promise(resolve => {
  setTimeout(() => {
    resolve();
  }, 500);
});

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('Component', () => {
  it('should mount and request users list', async () => {
    nock(/[.]+/)
      .get('/api/users?page=1&per_page=5')
      .reply(200, users, {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
      });

    const store = mockStore(reducer());
    const component = mount(
      <Provider store={store}>
        <Users />
      </Provider>,
    );
    const button = component.find('button');
    expect(button.length).toBe(1);
    await sleep;
    const actionsList = store.getActions();

    expect(actionsList).toContainEqual({
      type: types.GET_LIST_REQUEST,
    });

    expect(actionsList).toContainEqual({
      type: types.GET_LIST_SUCCESS,
      payload: users,
    });
  });

  it('should render first page when it loaded', async () => {
    nock(/[.]+/)
      .get('/api/users?page=2&per_page=5')
      .reply(200);

    const store = mockStore(page1);
    const component = mount(
      <Provider store={store}>
        <Users />
      </Provider>,
    );
    const userComponents = component.find('.User');
    expect(userComponents.length).toBe(5);
    const button = component.find('button');
    button.simulate('click');

    const actionsList = store.getActions();
    expect(actionsList).toContainEqual({
      type: types.GET_LIST_REQUEST,
    });
    const requests = actionsList.filter(item => item.type === types.GET_LIST_REQUEST);
    expect(requests.length).toBe(2);
  });

  it('When last page loaded button should dissapear', async () => {
    nock(/[.]+/)
      .get('/api/users?page=4&per_page=5')
      .reply(200);

    const store = mockStore(page2);
    const component = mount(
      <Provider store={store}>
        <Users />
      </Provider>,
    );
    const button = component.find('button');
    expect(button.length).toBe(0);
  });

  it('More button should be disabled while loading', async () => {
    nock(/[.]+/)
      .get('/api/users?page=1&per_page=5')
      .reply(200);

    const store = mockStore(page3);
    const component = mount(
      <Provider store={store}>
        <Users />
      </Provider>,
    );
    const button = component.find('button');

    expect(button.prop('disabled')).toBe(true);
  });
});
