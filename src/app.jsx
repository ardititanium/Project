import React from 'react';
import { render } from 'react-dom';
import { I18nextProvider } from 'react-i18next';
import { Provider } from 'react-redux';
import i18n from 'components/i18n';
import store from 'store/index';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import moment from 'moment';
import 'styles/fonts.pcss';
import 'styles/global.pcss';
import Home from 'pages/Home';

moment.locale(i18n.language);
const root = document.getElementById('page');

if (root !== null) {
  render(
    <I18nextProvider i18n={i18n}>
      <Provider store={store}>
        <Router>
          <Switch>
            <Route exact path="/" component={Home} />
          </Switch>
        </Router>
      </Provider>
    </I18nextProvider>,
    root,
  );
}
