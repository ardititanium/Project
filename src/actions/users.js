import fetch from 'isomorphic-fetch';
import * as types from 'types';

export function request() {
  return {
    type: types.users.GET_LIST_REQUEST,
  };
}

export function success(data) {
  return {
    type: types.users.GET_LIST_SUCCESS,
    payload: data,
  };
}

export function fail() {
  return {
    type: types.users.GET_LIST_FAIL,
  };
}

async function fetching(dispatch, getState) {
  dispatch(request());

  const previousState = getState().users;
  const { pageCount, perPage, currentPage } = previousState;
  const nextPage = currentPage + 1;

  if (nextPage > pageCount) {
    dispatch(fail());
    return;
  }

  try {
    const response = await fetch(
      `https://reqres.in/api/users?page=${nextPage}&per_page=${perPage}`,
    );
    const json = await response.json();
    dispatch(success(json));
  } catch (error) {
    dispatch(fail());
  }
}

export function fetchData() {
  return fetching;
}
